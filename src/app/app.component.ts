import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'legalstart';
  genders = [
    {value: 'female', viewValue: 'Mme.'},
    {value: 'male', viewValue: 'M.'}
  ];
  countries = [
    {value: 'brazil', viewValue: 'Brazil'},
    {value: 'france', viewValue: 'France'}
  ];
  
  brazilPassportRegex = `^[CFGHJKLMNPRTVWXYZ]{2}[0-9]{6}$`;
  francePassportRegex = `^[CFGHJKLMNPRTVWXYZ0-9]{9}$`;
  validationMessages = {
    'gender': {
      'required': 'le gender est obligatoire.',
    },
    'firstname': {
      'required': 'le prénom est obligatoire.',
    },
    'lastname': {
      'required': 'le nom est obligatoire.'
    },
    'country': {
      'required': 'le pays est obligatoire',

    },
    'passportId': {
      'required': 'l\'identifiant du passeport est obligatoire',
      'structure': 'numéro de passeport incorrecte',
    },
    'email': {
      'required': 'l\'email est obligatoire.',
      'structure': 'email incorrecte'
    }
  };

  fieldsValidity = {
    'gender' : { 'required' : true },
    'firstname': { 'required' : true },
    'lastname': { 'required' : true },
    'country': { 'required' : true },
    'passportId': { 'required' : true, 'format' : true },
    'email': { 'required' : true }
  };


  personalData : FormGroup;
  constructor(){}

  ngOnInit() {
    this.personalData = new FormGroup({
      gender   : new FormControl(null, [Validators.required]),
      firstname: new FormControl(null, [Validators.required]),
      lastname : new FormControl(null, [Validators.required]),
      country: new FormControl(null, [Validators.required]),
      passportId    : new FormControl(null, [Validators.required, Validators.pattern(this.francePassportRegex)]),
      email    : new FormControl(null, [Validators.required])
    });
   

  }
  onBlur(controlName : string){
    // console.log(this.personalData.controls[controlName].errors);
    if(this.personalData.controls[controlName].errors){
      if(this.personalData.controls[controlName].errors.required)
        this.fieldsValidity[controlName]['required'] = false;
      else 
        this.fieldsValidity[controlName]['required']  =true;
      if(controlName == 'passportId' && this.personalData.controls[controlName].valid == false)
        this.fieldsValidity[controlName]['format'] = false;
      else
        this.fieldsValidity[controlName]['format'] = true;

    }
   
  }


  updateFormControlValidator(event : any){
    // console.log(event)
    // console.log(this.personalData.controls)
    if(event == 'brazil')
      this.personalData.setControl('passportId', new FormControl(null, [Validators.required, Validators.pattern(this.brazilPassportRegex)]));
    if(event == 'france')
      this.personalData.setControl('passportId', new FormControl(null, [Validators.required, Validators.pattern(this.francePassportRegex)]));

 
    }
  
}
